package org.fundacionjala.at04.teamtetris.controllers;

import java.util.List;
import java.util.Random;

import javafx.scene.layout.BorderPane;
import org.fundacionjala.at04.teamtetris.models.BoardModel;
import org.fundacionjala.at04.teamtetris.models.TileModel;
import org.fundacionjala.at04.teamtetris.models.tetromino.RandomTetrominoGenerator;
import org.fundacionjala.at04.teamtetris.models.tetromino.TetrominoGenerator;
import org.fundacionjala.at04.teamtetris.views.gamescreen.GameScreenMainView;


/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class GameScreenController {

  private BoardModel model;
  private GameScreenMainView view;

  /**
   * Default Constructor
   *
   * @param model BoardModel.
   * @param view  BoardView.
   */
  public GameScreenController(BoardModel model, GameScreenMainView view) {
    this.model = model;
    this.view = view;
  }

  /**
   * Draw GameScreen.
   *
   * @return BorderPane.
   */
  public BorderPane drawGameScreen() {
    //Just for adding 10 pieces
    for (int i = 0; i < 10; i++) {
      Random rand = new Random();
      int x = rand.nextInt(9);
      int y = rand.nextInt(5) + 14;
      TetrominoGenerator tetromino = new RandomTetrominoGenerator();
      List<List<TileModel>> tetrominoList = tetromino.getTetromino().getTetrominoShape();
      int randomTetromino = tetrominoList.size();
      List<TileModel> tetrominoShape = tetrominoList.get(rand.nextInt(randomTetromino));
      if (!model.checkCollision(tetrominoShape, x, y)
          && !model.checkOutOfBorder(tetrominoShape, x, y)) {
        model.merge(tetrominoShape, x, y);
      } else {
        i--;
      }
    }
    view.drawCenterBoard(model);
    return view.drawGameScreenView();
  }
}
