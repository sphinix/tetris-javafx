package org.fundacionjala.at04.teamtetris.models.tetromino;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Abel on 02/07/2017.
 */
public class RandomTetrominoGenerator implements TetrominoGenerator {

  private List<TetrominoModel> tetrominosList;

  /**
   * Constructor for all Tetrominos.
   */
  public RandomTetrominoGenerator() {
    tetrominosList = new ArrayList<>();
    tetrominosList.add(new ITetromino());
    tetrominosList.add(new JTetromino());
    tetrominosList.add(new LTetromino());
    tetrominosList.add(new OTetromino());
    tetrominosList.add(new STetromino());
    tetrominosList.add(new TTetromino());
    tetrominosList.add(new ZTetromino());
  }

  /**
   * Get A Random Tetromino.
   *
   * @return Tetromino.
   */
  @Override
  public TetrominoModel getTetromino() {
    return tetrominosList.get(ThreadLocalRandom.current().nextInt(tetrominosList.size()));
  }
}
