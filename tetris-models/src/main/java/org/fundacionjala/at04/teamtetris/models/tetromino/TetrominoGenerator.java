package org.fundacionjala.at04.teamtetris.models.tetromino;

/**
 * Created by Abel on 02/07/2017.
 */
public interface TetrominoGenerator {

  TetrominoModel getTetromino();
}
