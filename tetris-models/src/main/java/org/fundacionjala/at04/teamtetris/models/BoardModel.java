package org.fundacionjala.at04.teamtetris.models;

import java.util.List;

/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public interface BoardModel {

  List<TileModel> getBoard();

  Integer getRows();

  Integer getColumns();

  void merge(List<TileModel> tileModels, Integer x, Integer y);

  boolean checkCollision(List<TileModel> tileModels, Integer x, Integer y);

  boolean checkOutOfBorder(List<TileModel> tileModels, Integer x, Integer y);

  boolean createNewTetromino();
}
