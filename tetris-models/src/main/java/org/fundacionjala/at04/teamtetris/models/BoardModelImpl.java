package org.fundacionjala.at04.teamtetris.models;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.fundacionjala.at04.teamtetris.models.tetromino.RandomTetrominoGenerator;
import org.fundacionjala.at04.teamtetris.models.tetromino.TetrominoGenerator;



/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class BoardModelImpl implements BoardModel {

  private final int rows;
  private final int columns;
  private List<TileModel> board;
  private TetrominoGenerator tetrominoGenerator;

  /**
   * Default Constructor.
   *
   * @param rows    Integer.
   * @param columns Integer.
   */
  public BoardModelImpl(int rows, int columns) {
    this.rows = rows;
    this.columns = columns;
    initBoardMatrix();
    tetrominoGenerator = new RandomTetrominoGenerator();
  }

  /**
   * Initial Board Matrix.
   */
  private void initBoardMatrix() {
    board = new ArrayList<>();
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.columns; j++) {
        IPosition position = new BrickPosition(i, j);
        TileModel tile = new ConcreteTileModel(position);
        board.add(tile);
      }
    }
  }

  /**
   * Check if Tiles are Out of Border.
   *
   * @param tileModels List.
   * @param x          Integer.
   * @param y          Integer.
   * @return Boolean.
   */
  @Override
  public boolean checkOutOfBorder(List<TileModel> tileModels, Integer x, Integer y) {
    for (TileModel tetrominoTile : tileModels) {
      if (tetrominoTile.getCol() + x > columns - 1 || tetrominoTile.getCol() + x < 0
          || tetrominoTile.getRow() + y > rows - 1 || tetrominoTile.getRow() + y < 0) {
        return true;
      }
    }
    return false;
  }

  /**
   * Check if Tiles has any Collision.
   *
   * @param tileModels List.
   * @param x          Integer.
   * @param y          Integer.
   * @return Boolean.
   */
  @Override
  public boolean checkCollision(List<TileModel> tileModels, Integer x, Integer y) {
    for (TileModel tetrominoTile : tileModels) {
      for (TileModel boardTile : board) {
        if (tetrominoTile.getCol() + x == boardTile.getCol() && tetrominoTile.getRow() + y
            == boardTile.getRow() && boardTile.getBrick() != null) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Merge a Tile into the Board.
   *
   * @param tileModels List.
   * @param x          Integer.
   * @param y          Integer.
   */
  @Override
  public void merge(List<TileModel> tileModels, Integer x, Integer y) {
    for (TileModel tetrominoTile : tileModels) {
      for (TileModel boardTile : board) {
        if (tetrominoTile.getCol() + x == boardTile.getCol() && tetrominoTile.getRow() + y
            == boardTile.getRow()) {
          boardTile.setBrick(tetrominoTile.getBrick());
          boardTile.setColor(tetrominoTile.getBrick().getColor());
        }
      }
    }
  }

  /**
   * Return the Board matrix.
   *
   * @return TileModel List.
   */
  @Override
  public List<TileModel> getBoard() {
    return board;
  }

  /**
   * Create a random Tetromino.
   *
   * @return Boolean.
   */
  @Override
  public boolean createNewTetromino() {
    List<List<TileModel>> tetrominoList = tetrominoGenerator.getTetromino().getTetrominoShape();
    int random = ThreadLocalRandom.current().nextInt(tetrominoList.size());
    List<TileModel> tetrominoShape = tetrominoList.get(random);
    return !checkCollision(tetrominoShape, 4, 0);
  }

  /**
   * Get Rows.
   *
   * @return Integer.
   */
  @Override
  public Integer getRows() {
    return rows;
  }

  /**
   * Get Columns.
   *
   * @return Integer.
   */
  @Override
  public Integer getColumns() {
    return columns;
  }
}
