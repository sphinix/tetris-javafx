package org.fundacionjala.at04.teamtetris.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.fundacionjala.at04.teamtetris.models.tetromino.OTetromino;
import org.fundacionjala.at04.teamtetris.models.tetromino.TetrominoModel;
import org.fundacionjala.at04.teamtetris.models.tetromino.ZTetromino;
import org.junit.Test;

/**
 * Created by abelb on 7/11/2017.
 */
public class BoardModelTest {

  /**
   * Test Initial Board.
   */
  @Test
  public void testNewBoard_InitialBoard() {
    BoardModel boardModel = new BoardModelImpl(20, 10);

    int expectedResult = 200;

    assertEquals(expectedResult, boardModel.getBoard().size());
  }

  /**
   * Test Board Columns and Rows.
   */
  @Test
  public void testNewBoard_GetColsAndRows() {
    Integer rows = 20;
    Integer cols = 10;
    BoardModel boardModel = new BoardModelImpl(rows, cols);

    assertEquals(rows, boardModel.getRows());
    assertEquals(cols, boardModel.getColumns());
  }

  /**
   * Test Merge a Tetromino.
   */
  @Test
  public void testMerge_TetrominoToEmptyBoard() {
    BoardModel boardModel = new BoardModelImpl(4, 4);

    TetrominoModel tetromino = new OTetromino();
    boardModel.merge(tetromino.getTetrominoShape().get(0), 0, 0);

    assertNotNull(boardModel.getBoard().get(0).getBrick());
    assertNotNull(boardModel.getBoard().get(1).getBrick());
    assertNotNull(boardModel.getBoard().get(4).getBrick());
    assertNotNull(boardModel.getBoard().get(5).getBrick());
    assertNull(boardModel.getBoard().get(2).getBrick());
    assertNull(boardModel.getBoard().get(3).getBrick());
  }

  /**
   * Test Collision with empty board.
   */
  @Test
  public void testCollision_WhenBoardIsNotEmpty() {
    BoardModel boardModel = new BoardModelImpl(4, 4);
    TetrominoModel tetromino = new OTetromino();
    boardModel.merge(tetromino.getTetrominoShape().get(0), 0, 0);

    TetrominoModel tetrominoOver = new ZTetromino();

    assertTrue(boardModel.checkCollision(tetrominoOver.getTetrominoShape().get(0), 0, 0));
  }

  /**
   * Test Collision with Board not empy.
   */
  @Test
  public void testCollision_WhenBoardIsEmpty() {
    BoardModel boardModel = new BoardModelImpl(4, 4);

    TetrominoModel tetromino = new OTetromino();

    assertFalse(boardModel.checkCollision(tetromino.getTetrominoShape().get(0), 0, 0));
  }

  /**
   * Test Tetromino is Out of Borders.
   */
  @Test
  public void testOutOfBorder_EmptyBoard() {
    BoardModel boardModel = new BoardModelImpl(4, 4);

    TetrominoModel tetromino = new OTetromino();

    assertTrue(boardModel.checkOutOfBorder(tetromino.getTetrominoShape().get(0), 0, 3));
  }

  /**
   * Test Tetromino is not Out of Borders.
   */
  @Test
  public void testOutOfBorder_EmptyBoardTwo() {
    BoardModel boardModel = new BoardModelImpl(4, 4);

    TetrominoModel tetromino = new OTetromino();

    assertFalse(boardModel.checkOutOfBorder(tetromino.getTetrominoShape().get(0), 0, 2));
  }

  /**
   * Test Can create a new Tetromino on Top Center.
   */
  @Test
  public void testCreateTetromino_EmptyBoard() {
    BoardModel boardModel = new BoardModelImpl(20, 10);

    assertTrue(boardModel.createNewTetromino());
  }
}
