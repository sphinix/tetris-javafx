package org.fundacionjala.at04.teamtetris.views.gamescreen;

import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import org.fundacionjala.at04.teamtetris.models.BoardModel;
import org.fundacionjala.at04.teamtetris.views.gamescreen.tetrisboard.TetrisBoard;
import org.fundacionjala.at04.teamtetris.views.gamescreen.tetrisboard.TetrisBoardView;

/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class GameScreenView extends BorderPane implements GameScreenMainView {

  private VBox leftSidePane;
  private VBox rightSidePane;
  private TetrisBoard tetrisBoard;

  /**
   * Default Constructor.
   */
  public GameScreenView() {
    setPrefSize(500, 500);
    tetrisBoard = new TetrisBoardView(25);
    leftSidePane = new VBox(200);
    rightSidePane = new VBox(200);
    setLeft(leftSidePane);
    setRight(rightSidePane);
    setCenter(tetrisBoard.getTetrisPane());
  }

  /**
   * Draw the Tetris Board.
   */
  @Override
  public void drawCenterBoard(BoardModel board) {
    setMargin(tetrisBoard.getTetrisPane(), new Insets(5, 5, 5, 5));
    tetrisBoard.drawTetrisBoard(board);
  }

  /**
   * Draw Right Side Panel.
   */
  public void drawRightBoard() {
    rightSidePane.setMinWidth(120);
    rightSidePane.setMinHeight(500);
    rightSidePane.getChildren().add(new PanelView("Next"));
    rightSidePane.getChildren().add(new PanelView("Lines Sent"));
  }

  /**
   * Draw Left Side Panel.
   */
  public void drawLeftBoard() {
    leftSidePane.setMinWidth(120);
    leftSidePane.setMinHeight(500);
    leftSidePane.getChildren().add(new PanelView("Hold"));
    leftSidePane.getChildren().add(new PanelView("Level"));
  }

  /**
   * Return the Game Screen View.
   *
   * @return BorderPane.
   */
  @Override
  public GameScreenView drawGameScreenView() {
    drawRightBoard();
    drawLeftBoard();
    return this;
  }
}
