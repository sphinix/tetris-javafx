package org.fundacionjala.at04.teamtetris.views.gamescreen;

import javafx.scene.control.TitledPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by AbelBarrientos on 6/27/2017.
 */
public class PanelView extends TitledPane {

  private String title;

  /**
   * Default Constructor.
   * @param title String.
   */
  public PanelView(String title) {
    this.title = title;
    setText(title);
    Rectangle pane = new Rectangle(125, 125, Color.TRANSPARENT);
    setContent(pane);
    setWidth(125);
    setHeight(125);
  }

  /**
   * Getter for title.
   * @return String.
   */
  public String getTitle() {
    return title;
  }
}
