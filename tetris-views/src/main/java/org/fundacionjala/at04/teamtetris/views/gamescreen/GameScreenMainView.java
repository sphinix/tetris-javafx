package org.fundacionjala.at04.teamtetris.views.gamescreen;


import org.fundacionjala.at04.teamtetris.models.BoardModel;

/**
 * Created by AbelBarrientos on 6/28/2017.
 */
public interface GameScreenMainView {

  GameScreenView drawGameScreenView();

  void drawCenterBoard(BoardModel board);
}
